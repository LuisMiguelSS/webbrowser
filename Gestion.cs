﻿using Navegador;
using System.Collections.Generic;
using System.Windows.Forms;

namespace NavegadorWeb
{
    static class Gestion
    {
        //
        // Atributos
        //
        public static NavegadorForm formularioInicial;
        public static FavouritesWindow formularioFavoritos;

        //
        // Gestión de Propiedades
        //
        public static string GetPaginaInicio() { return Properties.Settings.Default.homePage.ToString(); }
        public static List<string> GetListaFavoritos() { return Properties.Settings.Default.favourites; }
        public static void AddFavorito(string url)
        {
            if (GetListaFavoritos() == null)
            {
                Properties.Settings.Default["favourites"] = new List<string>();
                Properties.Settings.Default.favourites.Add(url);
            }
            else if (!Properties.Settings.Default.favourites.Contains(url))
                Properties.Settings.Default.favourites.Add(url);
        }

        //
        // Gestión Favoritos
        //
        public static Control GetFilaIndice(this TableLayoutPanel tabla, int indice)
        {
            return tabla.Controls[indice];
        }
        public static void AddFila(this TableLayoutPanel tabla, string url)
        {

            // Quitar fila de información de ausencia de datos
            if (tabla.RowCount == 2 && tabla.Controls[1].Text.Equals("Actualmente no tienes ninguna dirección guardada como favorita."))
            {
                tabla.Controls.RemoveAt(1);
                tabla.RowStyles.RemoveAt(1);
                tabla.RowCount = 1;
            }

            // TextBox donde se guarda la URL
            TextBox dato = new TextBox();
            dato.Dock = DockStyle.Fill;
            dato.ScrollBars = ScrollBars.Horizontal;
            dato.BorderStyle = BorderStyle.None;
            dato.ReadOnly = true;

            dato.Text = url == null ? "URL desconocida" : url;

            // Añadir nueva fila
            tabla.RowCount++;
            tabla.RowStyles.Add(new RowStyle(SizeType.Absolute, dato.Height + 5F));

            tabla.Controls.Add(dato, 0, tabla.RowCount - 1);

            // Listener para abrir URL
            dato.KeyPress += URL_KeyPress;
        }

        //
        // Listeners
        //
        private static void URL_KeyPress(object sender, KeyPressEventArgs e)
        {
            formularioInicial.NavegarPestanaActual(((TextBox)sender).Text);
        }
    }
}
