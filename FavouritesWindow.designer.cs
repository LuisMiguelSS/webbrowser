﻿using System.Windows.Forms;

namespace Navegador
{
    partial class FavouritesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FavouritesWindow));
            this.panel_tablaFavoritos = new System.Windows.Forms.TableLayoutPanel();
            this.label_tituloTabla = new System.Windows.Forms.Label();
            this.label_textoDefecto = new System.Windows.Forms.Label();
            this.panel_tablaFavoritos.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_tablaFavoritos
            // 
            this.panel_tablaFavoritos.AutoScroll = true;
            this.panel_tablaFavoritos.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.panel_tablaFavoritos.ColumnCount = 1;
            this.panel_tablaFavoritos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panel_tablaFavoritos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panel_tablaFavoritos.Controls.Add(this.label_tituloTabla, 0, 0);
            this.panel_tablaFavoritos.Controls.Add(this.label_textoDefecto, 0, 1);
            this.panel_tablaFavoritos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_tablaFavoritos.Location = new System.Drawing.Point(0, 0);
            this.panel_tablaFavoritos.Margin = new System.Windows.Forms.Padding(4);
            this.panel_tablaFavoritos.Name = "panel_tablaFavoritos";
            this.panel_tablaFavoritos.RowCount = 2;
            this.panel_tablaFavoritos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panel_tablaFavoritos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.panel_tablaFavoritos.Size = new System.Drawing.Size(323, 321);
            this.panel_tablaFavoritos.TabIndex = 0;
            // 
            // label_tituloTabla
            // 
            this.label_tituloTabla.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label_tituloTabla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_tituloTabla.Font = new System.Drawing.Font("Calibri", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tituloTabla.Location = new System.Drawing.Point(6, 2);
            this.label_tituloTabla.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_tituloTabla.Name = "label_tituloTabla";
            this.label_tituloTabla.Size = new System.Drawing.Size(311, 40);
            this.label_tituloTabla.TabIndex = 0;
            this.label_tituloTabla.Text = "Lista de direcciones favoritas";
            this.label_tituloTabla.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_textoDefecto
            // 
            this.label_textoDefecto.AutoSize = true;
            this.label_textoDefecto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_textoDefecto.Location = new System.Drawing.Point(6, 44);
            this.label_textoDefecto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_textoDefecto.Name = "label_textoDefecto";
            this.label_textoDefecto.Size = new System.Drawing.Size(311, 275);
            this.label_textoDefecto.TabIndex = 1;
            this.label_textoDefecto.Text = "Actualmente no tienes ninguna dirección guardada como favorita.";
            this.label_textoDefecto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FavouritesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 321);
            this.Controls.Add(this.panel_tablaFavoritos);
            this.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FavouritesWindow";
            this.Text = "Favoritos";
            this.panel_tablaFavoritos.ResumeLayout(false);
            this.panel_tablaFavoritos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel panel_tablaFavoritos;
        private Label label_tituloTabla;
        private Label label_textoDefecto;
    }
}