﻿using NavegadorWeb;
using System.Windows.Forms;

namespace Navegador
{
    public partial class FavouritesWindow : Form
    {
        public FavouritesWindow()
        {
            InitializeComponent();


            // Configurar tabla
            if(Gestion.GetListaFavoritos() != null)
                foreach (string url in Gestion.GetListaFavoritos())
                    panel_tablaFavoritos.AddFila(url);


            // Establecer tabla en Gestion
            Gestion.formularioFavoritos = this;
        }

        //
        // Getters
        //
        public TableLayoutPanel GetTabla() { return panel_tablaFavoritos; }

    }
}
