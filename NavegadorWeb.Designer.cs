﻿namespace Navegador
{
    partial class NavegadorForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NavegadorForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.panelPestanas = new System.Windows.Forms.TabControl();
            this.tabInicio = new System.Windows.Forms.TabPage();
            this.navegadorInicio = new System.Windows.Forms.WebBrowser();
            this.tabAnadirPestana = new System.Windows.Forms.TabPage();
            this.barra_menu = new System.Windows.Forms.MenuStrip();
            this.menu_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_guardarComo = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_vistaPrevia = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_configurarPagina = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Imprimir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSeparador1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_salir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_configuracion = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_favoritosMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_paginaInicio = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_busquedaMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_propiedades = new System.Windows.Forms.ToolStripMenuItem();
            this.historial = new System.Windows.Forms.ToolStripComboBox();
            this.barra_herramientas = new System.Windows.Forms.ToolStrip();
            this.btn_atras = new System.Windows.Forms.ToolStripButton();
            this.btn_adelante = new System.Windows.Forms.ToolStripButton();
            this.btn_recargar = new System.Windows.Forms.ToolStripButton();
            this.menuSeparador2 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_inicio = new System.Windows.Forms.ToolStripButton();
            this.btn_verFavoritos = new System.Windows.Forms.ToolStripButton();
            this.menuSeparador3 = new System.Windows.Forms.ToolStripSeparator();
            this.label_Buscar = new System.Windows.Forms.ToolStripLabel();
            this.input_buscar = new System.Windows.Forms.ToolStripTextBox();
            this.btn_buscar = new System.Windows.Forms.ToolStripButton();
            this.btn_anadirFavorito = new System.Windows.Forms.ToolStripButton();
            this.btnCerrarPestana = new System.Windows.Forms.ToolStripButton();
            this.input_buscarGoogle = new System.Windows.Forms.ToolStripTextBox();
            this.btnBuscarGoogle = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.panelPestanas.SuspendLayout();
            this.tabInicio.SuspendLayout();
            this.barra_menu.SuspendLayout();
            this.barra_herramientas.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.panelPestanas);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(800, 433);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(800, 485);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.barra_menu);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.barra_herramientas);
            // 
            // panelPestanas
            // 
            this.panelPestanas.Controls.Add(this.tabInicio);
            this.panelPestanas.Controls.Add(this.tabAnadirPestana);
            this.panelPestanas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPestanas.Location = new System.Drawing.Point(0, 0);
            this.panelPestanas.Name = "panelPestanas";
            this.panelPestanas.SelectedIndex = 0;
            this.panelPestanas.Size = new System.Drawing.Size(800, 433);
            this.panelPestanas.TabIndex = 0;
            this.panelPestanas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelPestanas_MouseDown);
            // 
            // tabInicio
            // 
            this.tabInicio.Controls.Add(this.navegadorInicio);
            this.tabInicio.Location = new System.Drawing.Point(4, 23);
            this.tabInicio.Name = "tabInicio";
            this.tabInicio.Padding = new System.Windows.Forms.Padding(3);
            this.tabInicio.Size = new System.Drawing.Size(792, 406);
            this.tabInicio.TabIndex = 0;
            this.tabInicio.Text = "Inicio";
            this.tabInicio.UseVisualStyleBackColor = true;
            // 
            // navegadorInicio
            // 
            this.navegadorInicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navegadorInicio.Location = new System.Drawing.Point(3, 3);
            this.navegadorInicio.Name = "navegadorInicio";
            this.navegadorInicio.Size = new System.Drawing.Size(786, 400);
            this.navegadorInicio.TabIndex = 0;
            this.navegadorInicio.Url = new System.Uri("https://www.google.com", System.UriKind.Absolute);
            this.navegadorInicio.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.NavegadorInicial_DocumentCompleted);
            // 
            // tabAnadirPestana
            // 
            this.tabAnadirPestana.Location = new System.Drawing.Point(4, 22);
            this.tabAnadirPestana.Name = "tabAnadirPestana";
            this.tabAnadirPestana.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnadirPestana.Size = new System.Drawing.Size(792, 407);
            this.tabAnadirPestana.TabIndex = 1;
            this.tabAnadirPestana.Text = "+";
            this.tabAnadirPestana.UseVisualStyleBackColor = true;
            // 
            // barra_menu
            // 
            this.barra_menu.Dock = System.Windows.Forms.DockStyle.None;
            this.barra_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Archivo,
            this.menu_configuracion,
            this.menu_propiedades,
            this.historial});
            this.barra_menu.Location = new System.Drawing.Point(0, 0);
            this.barra_menu.Name = "barra_menu";
            this.barra_menu.Size = new System.Drawing.Size(800, 27);
            this.barra_menu.TabIndex = 0;
            this.barra_menu.Text = "menuStrip1";
            // 
            // menu_Archivo
            // 
            this.menu_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_guardarComo,
            this.btn_vistaPrevia,
            this.btn_configurarPagina,
            this.btn_Imprimir,
            this.menuSeparador1,
            this.btn_salir});
            this.menu_Archivo.Name = "menu_Archivo";
            this.menu_Archivo.Size = new System.Drawing.Size(60, 23);
            this.menu_Archivo.Text = "Archivo";
            // 
            // btn_guardarComo
            // 
            this.btn_guardarComo.Image = global::NavegadorWeb.Properties.Resources.SaveAs_16x;
            this.btn_guardarComo.Name = "btn_guardarComo";
            this.btn_guardarComo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.btn_guardarComo.Size = new System.Drawing.Size(221, 22);
            this.btn_guardarComo.Text = "Guardar como...";
            this.btn_guardarComo.Click += new System.EventHandler(this.btn_guardarComo_Click);
            // 
            // btn_vistaPrevia
            // 
            this.btn_vistaPrevia.Image = global::NavegadorWeb.Properties.Resources.PrintPreview_16x;
            this.btn_vistaPrevia.Name = "btn_vistaPrevia";
            this.btn_vistaPrevia.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.V)));
            this.btn_vistaPrevia.Size = new System.Drawing.Size(221, 22);
            this.btn_vistaPrevia.Text = "Vista previa";
            this.btn_vistaPrevia.Click += new System.EventHandler(this.btn_vistaPrevia_Click);
            // 
            // btn_configurarPagina
            // 
            this.btn_configurarPagina.Image = global::NavegadorWeb.Properties.Resources.ConfigurationEditor_16x;
            this.btn_configurarPagina.Name = "btn_configurarPagina";
            this.btn_configurarPagina.Size = new System.Drawing.Size(221, 22);
            this.btn_configurarPagina.Text = "Configurar página...";
            this.btn_configurarPagina.Click += new System.EventHandler(this.btn_configurarPagina_Click);
            // 
            // btn_Imprimir
            // 
            this.btn_Imprimir.Image = global::NavegadorWeb.Properties.Resources.Print_16x;
            this.btn_Imprimir.Name = "btn_Imprimir";
            this.btn_Imprimir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.btn_Imprimir.Size = new System.Drawing.Size(221, 22);
            this.btn_Imprimir.Text = "Imprimir...";
            this.btn_Imprimir.Click += new System.EventHandler(this.btn_Imprimir_Click);
            // 
            // menuSeparador1
            // 
            this.menuSeparador1.Name = "menuSeparador1";
            this.menuSeparador1.Size = new System.Drawing.Size(218, 6);
            // 
            // btn_salir
            // 
            this.btn_salir.Image = global::NavegadorWeb.Properties.Resources.Exit_16x;
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.btn_salir.Size = new System.Drawing.Size(221, 22);
            this.btn_salir.Text = "Salir";
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // menu_configuracion
            // 
            this.menu_configuracion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_favoritosMenu,
            this.btn_paginaInicio,
            this.btn_busquedaMenu});
            this.menu_configuracion.Name = "menu_configuracion";
            this.menu_configuracion.Size = new System.Drawing.Size(95, 23);
            this.menu_configuracion.Text = "Configuración";
            // 
            // btn_favoritosMenu
            // 
            this.btn_favoritosMenu.Image = global::NavegadorWeb.Properties.Resources.Favorite_16x;
            this.btn_favoritosMenu.Name = "btn_favoritosMenu";
            this.btn_favoritosMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.btn_favoritosMenu.Size = new System.Drawing.Size(232, 22);
            this.btn_favoritosMenu.Text = "Favoritos";
            this.btn_favoritosMenu.Click += new System.EventHandler(this.btn_favoritosMenu_Click);
            // 
            // btn_paginaInicio
            // 
            this.btn_paginaInicio.Image = global::NavegadorWeb.Properties.Resources.Home_16x;
            this.btn_paginaInicio.Name = "btn_paginaInicio";
            this.btn_paginaInicio.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Space)));
            this.btn_paginaInicio.Size = new System.Drawing.Size(232, 22);
            this.btn_paginaInicio.Text = "Página de inicio...";
            this.btn_paginaInicio.Click += new System.EventHandler(this.btn_paginaInicio_Click);
            // 
            // btn_busquedaMenu
            // 
            this.btn_busquedaMenu.Image = global::NavegadorWeb.Properties.Resources.Search_16x;
            this.btn_busquedaMenu.Name = "btn_busquedaMenu";
            this.btn_busquedaMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.btn_busquedaMenu.Size = new System.Drawing.Size(232, 22);
            this.btn_busquedaMenu.Text = "Búsqueda...";
            this.btn_busquedaMenu.Click += new System.EventHandler(this.btn_busquedaMenu_Click);
            // 
            // menu_propiedades
            // 
            this.menu_propiedades.Name = "menu_propiedades";
            this.menu_propiedades.Size = new System.Drawing.Size(84, 23);
            this.menu_propiedades.Text = "Propiedades";
            this.menu_propiedades.Click += new System.EventHandler(this.menu_propiedades_Click);
            // 
            // historial
            // 
            this.historial.Name = "historial";
            this.historial.Size = new System.Drawing.Size(121, 23);
            this.historial.Text = "Historial";
            this.historial.ToolTipText = "Historial";
            this.historial.DropDownClosed += new System.EventHandler(this.historial_DropDownClosed);
            // 
            // barra_herramientas
            // 
            this.barra_herramientas.CanOverflow = false;
            this.barra_herramientas.Dock = System.Windows.Forms.DockStyle.None;
            this.barra_herramientas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_atras,
            this.btn_adelante,
            this.btn_recargar,
            this.menuSeparador2,
            this.btn_inicio,
            this.btn_verFavoritos,
            this.menuSeparador3,
            this.label_Buscar,
            this.input_buscar,
            this.btn_buscar,
            this.btn_anadirFavorito,
            this.btnCerrarPestana,
            this.input_buscarGoogle,
            this.btnBuscarGoogle});
            this.barra_herramientas.Location = new System.Drawing.Point(3, 27);
            this.barra_herramientas.Name = "barra_herramientas";
            this.barra_herramientas.Size = new System.Drawing.Size(750, 25);
            this.barra_herramientas.TabIndex = 1;
            // 
            // btn_atras
            // 
            this.btn_atras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_atras.Image = global::NavegadorWeb.Properties.Resources.Backward_16x;
            this.btn_atras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_atras.Name = "btn_atras";
            this.btn_atras.Size = new System.Drawing.Size(23, 22);
            this.btn_atras.Text = "Atrás";
            this.btn_atras.Click += new System.EventHandler(this.btn_atras_Click);
            // 
            // btn_adelante
            // 
            this.btn_adelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_adelante.Image = global::NavegadorWeb.Properties.Resources.Forward_16x;
            this.btn_adelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_adelante.Name = "btn_adelante";
            this.btn_adelante.Size = new System.Drawing.Size(23, 22);
            this.btn_adelante.Text = "Adelante";
            this.btn_adelante.Click += new System.EventHandler(this.btn_adelante_Click);
            // 
            // btn_recargar
            // 
            this.btn_recargar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_recargar.Image = global::NavegadorWeb.Properties.Resources.Refresh_16x;
            this.btn_recargar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_recargar.Name = "btn_recargar";
            this.btn_recargar.Size = new System.Drawing.Size(23, 22);
            this.btn_recargar.Text = "Recargar";
            this.btn_recargar.Click += new System.EventHandler(this.btn_recargar_Click);
            // 
            // menuSeparador2
            // 
            this.menuSeparador2.Name = "menuSeparador2";
            this.menuSeparador2.Size = new System.Drawing.Size(6, 25);
            // 
            // btn_inicio
            // 
            this.btn_inicio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_inicio.Image = global::NavegadorWeb.Properties.Resources.Home_16x;
            this.btn_inicio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_inicio.Name = "btn_inicio";
            this.btn_inicio.Size = new System.Drawing.Size(23, 22);
            this.btn_inicio.Text = "Inicio";
            this.btn_inicio.Click += new System.EventHandler(this.btn_inicio_Click);
            // 
            // btn_verFavoritos
            // 
            this.btn_verFavoritos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_verFavoritos.Image = global::NavegadorWeb.Properties.Resources.Favorite_16x;
            this.btn_verFavoritos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_verFavoritos.Name = "btn_verFavoritos";
            this.btn_verFavoritos.Size = new System.Drawing.Size(23, 22);
            this.btn_verFavoritos.Text = "Ver Favoritos";
            this.btn_verFavoritos.Click += new System.EventHandler(this.btn_verFavoritos_Click);
            // 
            // menuSeparador3
            // 
            this.menuSeparador3.Name = "menuSeparador3";
            this.menuSeparador3.Size = new System.Drawing.Size(6, 25);
            // 
            // label_Buscar
            // 
            this.label_Buscar.Name = "label_Buscar";
            this.label_Buscar.Size = new System.Drawing.Size(45, 22);
            this.label_Buscar.Text = "Buscar:";
            // 
            // input_buscar
            // 
            this.input_buscar.Name = "input_buscar";
            this.input_buscar.Size = new System.Drawing.Size(370, 25);
            this.input_buscar.ToolTipText = "URL a seguir...";
            this.input_buscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.input_buscar_KeyPress);
            // 
            // btn_buscar
            // 
            this.btn_buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_buscar.Image = global::NavegadorWeb.Properties.Resources.Search_16x;
            this.btn_buscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(23, 22);
            this.btn_buscar.Text = "Ir";
            this.btn_buscar.ToolTipText = "Ir...";
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // btn_anadirFavorito
            // 
            this.btn_anadirFavorito.BackColor = System.Drawing.SystemColors.Control;
            this.btn_anadirFavorito.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_anadirFavorito.Image = global::NavegadorWeb.Properties.Resources.AddtoFavorites_16x;
            this.btn_anadirFavorito.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_anadirFavorito.Name = "btn_anadirFavorito";
            this.btn_anadirFavorito.Size = new System.Drawing.Size(23, 22);
            this.btn_anadirFavorito.Text = "Añadir a Favoritos";
            this.btn_anadirFavorito.Click += new System.EventHandler(this.btn_anadirFavorito_Click);
            // 
            // btnCerrarPestana
            // 
            this.btnCerrarPestana.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCerrarPestana.Image = global::NavegadorWeb.Properties.Resources.Close_red_16x;
            this.btnCerrarPestana.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCerrarPestana.Name = "btnCerrarPestana";
            this.btnCerrarPestana.Size = new System.Drawing.Size(23, 22);
            this.btnCerrarPestana.Text = "Cerrar pestaña";
            this.btnCerrarPestana.Click += new System.EventHandler(this.btnCerrarPestana_Click);
            // 
            // input_buscarGoogle
            // 
            this.input_buscarGoogle.Name = "input_buscarGoogle";
            this.input_buscarGoogle.Size = new System.Drawing.Size(100, 25);
            this.input_buscarGoogle.Text = "Google";
            this.input_buscarGoogle.ToolTipText = "Buscar....";
            this.input_buscarGoogle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.input_buscarGoogle_KeyPress);
            // 
            // btnBuscarGoogle
            // 
            this.btnBuscarGoogle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBuscarGoogle.Image = global::NavegadorWeb.Properties.Resources.SearchGo_16x;
            this.btnBuscarGoogle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBuscarGoogle.Name = "btnBuscarGoogle";
            this.btnBuscarGoogle.Size = new System.Drawing.Size(23, 22);
            this.btnBuscarGoogle.Text = "Buscar con Google";
            this.btnBuscarGoogle.Click += new System.EventHandler(this.btnBuscarGoogle_Click);
            // 
            // NavegadorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 485);
            this.Controls.Add(this.toolStripContainer1);
            this.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.barra_menu;
            this.Name = "NavegadorForm";
            this.Text = "Navegador Web";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NavegadorForm_FormClosing);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.panelPestanas.ResumeLayout(false);
            this.tabInicio.ResumeLayout(false);
            this.barra_menu.ResumeLayout(false);
            this.barra_menu.PerformLayout();
            this.barra_herramientas.ResumeLayout(false);
            this.barra_herramientas.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.MenuStrip barra_menu;
        private System.Windows.Forms.ToolStripMenuItem menu_Archivo;
        private System.Windows.Forms.ToolStripMenuItem menu_configuracion;
        private System.Windows.Forms.ToolStripMenuItem menu_propiedades;
        private System.Windows.Forms.TabControl panelPestanas;
        private System.Windows.Forms.TabPage tabInicio;
        private System.Windows.Forms.TabPage tabAnadirPestana;
        private System.Windows.Forms.ToolStrip barra_herramientas;
        private System.Windows.Forms.ToolStripButton btn_atras;
        private System.Windows.Forms.ToolStripButton btn_adelante;
        private System.Windows.Forms.ToolStripButton btn_recargar;
        private System.Windows.Forms.ToolStripButton btn_inicio;
        private System.Windows.Forms.ToolStripButton btn_verFavoritos;
        private System.Windows.Forms.ToolStripLabel label_Buscar;
        private System.Windows.Forms.ToolStripTextBox input_buscar;
        private System.Windows.Forms.WebBrowser navegadorInicio;
        private System.Windows.Forms.ToolStripMenuItem btn_guardarComo;
        private System.Windows.Forms.ToolStripMenuItem btn_vistaPrevia;
        private System.Windows.Forms.ToolStripMenuItem btn_configurarPagina;
        private System.Windows.Forms.ToolStripMenuItem btn_Imprimir;
        private System.Windows.Forms.ToolStripSeparator menuSeparador1;
        private System.Windows.Forms.ToolStripMenuItem btn_salir;
        private System.Windows.Forms.ToolStripMenuItem btn_favoritosMenu;
        private System.Windows.Forms.ToolStripMenuItem btn_paginaInicio;
        private System.Windows.Forms.ToolStripMenuItem btn_busquedaMenu;
        private System.Windows.Forms.ToolStripSeparator menuSeparador2;
        private System.Windows.Forms.ToolStripSeparator menuSeparador3;
        private System.Windows.Forms.ToolStripButton btn_buscar;
        private System.Windows.Forms.ToolStripButton btn_anadirFavorito;
        private System.Windows.Forms.ToolStripComboBox historial;
        private System.Windows.Forms.ToolStripButton btnCerrarPestana;
        private System.Windows.Forms.ToolStripTextBox input_buscarGoogle;
        private System.Windows.Forms.ToolStripButton btnBuscarGoogle;
    }
}

