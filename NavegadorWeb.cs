﻿using Microsoft.VisualBasic;
using NavegadorWeb;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Navegador
{
    public partial class NavegadorForm : Form
    {
        //
        // Atributos
        //
        public static string LUGAR_GUARDADO_DEFECTO = Environment.SpecialFolder.Desktop.ToString();
        public static string REGEX_URL = "^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$";
        public NavegadorForm()
        {
            InitializeComponent();

            // Establecer navegadorWeb en Gestion
            Gestion.formularioInicial = this;
        }

        //
        // Otros métodos
        //
        public WebBrowser GetNavegadorActual()
        {
            return (WebBrowser)panelPestanas.SelectedTab.Controls[0];
        }

        public int GetNumeroTabs() { return panelPestanas.TabPages.Count; }

        public void ActualizarPestana(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            ActualizarPestana();
        }
        public void ActualizarPestana()
        {
            panelPestanas.SelectedTab.Text = GetNavegadorActual().Document.Title; // Pestaña
            input_buscar.Text = GetNavegadorActual().Url.ToString(); // Barra de búsqueda

            // Actualizar historial
            if (historial.Items.Count < 10)
                historial.Items.Add(GetNavegadorActual().Url);
            else
                historial.Items.RemoveAt(historial.Items.Count - 1);
        }
        public void NavegarPestanaActual(string url) {
            GetNavegadorActual().Navigate(url);
        }

        //
        // Listeners
        //

        private void panelPestanas_MouseDown(object sender, MouseEventArgs e)
        {
            int lastIndex = panelPestanas.TabCount - 1;

            // Buscar click del ratón en el recuadro de la última pestaña
            if (panelPestanas.GetTabRect(lastIndex).Contains(e.Location))
            {
                // Crear navegador
                WebBrowser navegadorPlantilla = new WebBrowser();
                navegadorPlantilla.Dock = DockStyle.Fill;
                navegadorPlantilla.Url = new Uri(Gestion.GetPaginaInicio(), UriKind.Absolute);

                // Añadir nueva pestaña y nuevo navegador
                panelPestanas.TabPages.Insert(lastIndex, "Nueva pestaña");
                panelPestanas.TabPages[lastIndex].Controls.Add(navegadorPlantilla);
                panelPestanas.SelectedIndex = lastIndex;
                GetNavegadorActual().DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(ActualizarPestana);
            }
        }

        private void btn_guardarComo_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().ShowSaveAsDialog();
        }

        private void btn_vistaPrevia_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().ShowPrintPreviewDialog();
        }

        private void btn_configurarPagina_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().ShowPageSetupDialog();
        }

        private void btn_Imprimir_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().ShowPrintDialog();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void NavegadorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (GetNumeroTabs() > 2)
            {
                if (MessageBox.Show("Tienes varias pestañas abiertas, ¿estás seguro de que deseas cerrarlas?", "Cerrando", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void menu_propiedades_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().ShowPropertiesDialog();
        }

        private void btn_paginaInicio_Click(object sender, EventArgs e)
        {
            NavegadorWeb.Properties.Settings.Default["homePage"] = Interaction.InputBox("Nueva página de inicio:", "Configurar página de inicio", NavegadorWeb.Properties.Settings.Default["homePage"].ToString(), -1, -1);
        }

        private void btn_atras_Click(object sender, EventArgs e)
        {
            if (GetNavegadorActual().CanGoBack)
                GetNavegadorActual().GoBack();
        }

        private void btn_adelante_Click(object sender, EventArgs e)
        {
            if (GetNavegadorActual().CanGoForward)
                GetNavegadorActual().GoForward();
        }

        private void btn_recargar_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().Refresh();
        }

        private void btn_inicio_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().Navigate(Gestion.GetPaginaInicio());
        }

        private void btn_anadirFavorito_Click(object sender, EventArgs e)
        {
            if (GetNavegadorActual().Url.AbsoluteUri != null)
                Gestion.AddFavorito(GetNavegadorActual().Url.AbsoluteUri.ToString());
        }

        private void btn_verFavoritos_Click(object sender, EventArgs e)
        {
            new FavouritesWindow().ShowDialog();
        }

        private void btn_favoritosMenu_Click(object sender, EventArgs e)
        {
            btn_verFavoritos_Click(sender, e);
        }

        private void NavegadorInicial_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            // Evitar que cargue por duplicado
            if (GetNavegadorActual().ReadyState != WebBrowserReadyState.Complete)
                return;
            ActualizarPestana();
        }

        private void input_buscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            var resultadoRegex = Regex.Match(input_buscar.Text, REGEX_URL, RegexOptions.IgnoreCase);

            if (e.KeyChar == Convert.ToChar(Keys.Enter) && resultadoRegex.Success)
                GetNavegadorActual().Navigate(input_buscar.Text);
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            var resultadoRegex = Regex.Match(input_buscar.Text, REGEX_URL, RegexOptions.IgnoreCase);

            if (resultadoRegex.Success)
                GetNavegadorActual().Navigate(input_buscar.Text);
        }

        private void btnCerrarPestana_Click(object sender, EventArgs e)
        {
            if(panelPestanas.TabPages.Count > 2)
                panelPestanas.TabPages.Remove(panelPestanas.SelectedTab);
        }

        private void input_buscarGoogle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                GetNavegadorActual().Navigate("https://www.google.com/search?q=" + input_buscarGoogle.Text);
        }

        private void btnBuscarGoogle_Click(object sender, EventArgs e)
        {
            GetNavegadorActual().Navigate("https://www.google.com/search?q=" + input_buscarGoogle.Text);
        }

        private void historial_DropDownClosed(object sender, EventArgs e)
        {
            if(historial.SelectedItem != null)
                GetNavegadorActual().Navigate(historial.SelectedItem.ToString());
        }

        private void btn_busquedaMenu_Click(object sender, EventArgs e)
        {
            input_buscarGoogle.Focus();
        }
    }
}
